#!/bin/bash


VALID_COUNTRIES=("jp" "th" "id" "hk" "kh");
VALID_ENVIRONMENTS=("dev" "uat" "prod");
VALID_VERIFICATION_RESPONSES=("Y" "N" "y" "n");
API_NAME="notification"
APP_NAME="Moments Notification API Stack";
IMAGE_URI="607410213812.dkr.ecr.ap-southeast-1.amazonaws.com/test:dev-0f7b72c9";
AWS_REGION_NAME="ap-southeast-1";
printSeparator() {
    echo -e ""
    echo -e ""
}

printColored() {
    TEXT="${1}";

    printf "\e[38;2;248;138;0m${TEXT} \e[0m\n";
}

printError() {
    TEXT="${1}";

    printf "\e[38;2;255;0;0m${TEXT}\e[0m\n";
}

printListItem() {
    TEXT="${1}";
    CHECK_MARK="\e[38;2;0;255;0m\xE2\x9C\x94\e[0m";

    echo -e "${CHECK_MARK} ${TEXT}";
}

printHeader() {
    clear;
    printColored " ________          _______  ";
    printColored "|  ____\ \        / /  __ \ ";
    printColored "| |__   \ \  /\  / /| |  | |";
    printColored "|  __|   \ \/  \/ / | |  | |";
    printColored "| |       \  /\  /  | |__| |";
    printColored "|_|        \/  \/   |_____/ ";
}

collectCountry() {
  printHeader
  printSeparator;
  printColored "$APP_NAME";
  printSeparator;
  echo -n "0). Enter Country Code (${VALID_COUNTRIES[*]}): "
  read COUNTRY_CODE
}

while true; do
  collectCountry

  if [[ " ${VALID_COUNTRIES[*]} " == *" ${COUNTRY_CODE} "* ]];
  then
    break
  else
    printError "Invalid country code, try again after 1 second."
    sleep 1
    printSeparator
  fi
done
printSeparator;

collectEnvironment() {
  printHeader
  printSeparator;
  printColored "$APP_NAME";
  printSeparator;
  echo -n "1). Enter Environment Code (${VALID_ENVIRONMENTS[*]}): "
  read ENVIRONMENT_NAME
}

while true; do
  collectEnvironment

  if [[ " ${VALID_ENVIRONMENTS[*]} " == *" ${ENVIRONMENT_NAME} "* ]];
  then
    break
  else
    printError "Invalid environment code, try again after 1 second."
    sleep 1
    printSeparator
  fi
done
printSeparator;

collectAwsCliProfile() {
  printHeader
  printSeparator;
  printColored "$APP_NAME";
  printSeparator;
  echo -n "3). Enter AWS CLI Profile: "
  read AWS_CLI_PROFILE
}

collectAwsCliProfile;

AWS_CF_STACK_NAME="${COUNTRY_CODE}-${ENVIRONMENT_NAME}-${API_NAME}-apis";
SUBNET_IDS=$(aws ec2 describe-subnets --filters "Name=tag:Name,Values=fwd-moments-2.0-${ENVIRONMENT_NAME}-vpc-private-ap-southeast-1*" --profile moments --output json | jq -r '.Subnets|map(.SubnetId)|join(",")')


verifyInstallation() {
  printHeader;
  printSeparator;
  printColored "I am about to release an instance of $APP_NAME with the following settings";
  printSeparator;
  printListItem "ENVIRONMENT_NAME = ${ENVIRONMENT_NAME}";
  printListItem "API_NAME = ${API_NAME}";
  printListItem "AWS_CF_STACK_NAME = ${AWS_CF_STACK_NAME}";
  printListItem "IMAGE_URI = ${IMAGE_URI}";
  printListItem "AWS_REGION_NAME = ${AWS_REGION_NAME}";
  printListItem "AWS_CLI_PROFILE = ${AWS_CLI_PROFILE}";
  printListItem "SUBNET_IDS = ${SUBNET_IDS}";
  printSeparator;
  echo -n "3). Are you sure you want to proceed? ([Y]es, [N]o): "
  read CONFIRM_CODE
}

while true; do
  verifyInstallation

  if [[ " ${VALID_VERIFICATION_RESPONSES[*]} " == *" ${CONFIRM_CODE} "* ]];
  then
    break
  else
    printError "Invalid value."
    sleep 1
    printSeparator
  fi
done

if [[ $CONFIRM_CODE == "Y" ]] || [[ $CONFIRM_CODE == "y" ]]
then
  printSeparator
  printHeader
  printSeparator
  printColored "Initiating deployment...";
  printSeparator
else
  clear;

  printHeader
  printSeparator
  printError "Stack release aborted. Press any key to exit";
  read
  clear;
  exit 0;
fi

# For windows users 
UNAME=$(uname);

if [[ "$UNAME" == CYGWIN* || "$UNAME" == MINGW* ]] ; then
  echo "Windows machine detected..."
  alias sam='sam.cmd'
fi


# Package layers code.
# translate SAM template to plain CloudFormation.
sam package --template-file ./apis.sam.yaml \
  --image-repository 607410213812.dkr.ecr.ap-southeast-1.amazonaws.com/test \
  --region "${AWS_REGION_NAME}" \
  --output-template-file ./apis.cfn.yaml \
  --profile "${AWS_CLI_PROFILE}";

# Deploy packaged code.
sam deploy --template-file ./apis.cfn.yaml \
  --image-repository 607410213812.dkr.ecr.ap-southeast-1.amazonaws.com/test \
  --region "${AWS_REGION_NAME}" \
  --stack-name $AWS_CF_STACK_NAME \
  --parameter-overrides ParameterKey=Environment,ParameterValue=$ENVIRONMENT_NAME \
    ParameterKey=Country,ParameterValue=$COUNTRY_CODE \
    ParameterKey=ImageUri,ParameterValue=$IMAGE_URI \
    ParameterKey=SubnetIds,ParameterValue=$SUBNET_IDS \
    ParameterKey=ApiName,ParameterValue=$API_NAME \
  --capabilities CAPABILITY_IAM \
  --profile "${AWS_CLI_PROFILE}"

# Say congrats, offer beer and goodbye :)
printSeparator;
echo -n "Deployment complete! 🍺🍺🍺";
printSeparator;
read -n 1 -s -r -p "Press any key to continue....";
clear;
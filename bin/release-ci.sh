#!/bin/bash
SLIENT="$SLIENT";

. ./bin/utils.sh

if [[  ${SLIENT} != "1"  ]]; then

  echo "Enter Country Code (${VALID_COUNTRIES[*]}): ";
  read COUNTRY_CODE;
  if [[ " ${VALID_COUNTRIES[*]} " != *" ${COUNTRY_CODE} "* ]];
  then
    printListItem "Invalid COUNTRY_CODE, Please select from:";
    printListItem "$(printf '%s, ' ${VALID_COUNTRIES[@]})";
    exit 0;
  fi

  echo "Enter Environment Code (${VALID_ENVIRONMENTS[*]}): ";
  read ENVIRONMENT_NAME;
  if [[ " ${VALID_ENVIRONMENTS[*]} " != *" ${ENVIRONMENT_NAME} "* ]];
  then
    printListItem "Invalid ENVIRONMENT_NAME, Please select from:";
    printListItem "$(printf '%s, ' ${VALID_ENVIRONMENTS[@]})";
    exit 0;
  fi

  echo "Enter AWS CLI Profile: ";
  read AWS_CLI_PROFILE;
  
  AWS_REGION_NAME="$(aws configure get region --profile ${AWS_CLI_PROFILE})";

  USE_PROFILE="--profile ${AWS_CLI_PROFILE}"
fi

PROJECT_PATH=$(getProjectPath $COUNTRY_CODE);

SECRET_VERSION_ID="";
PREFIX="${COUNTRY_CODE}-${ENVIRONMENT_NAME}";
SOURCE_FILE="./envs/${COUNTRY_CODE}.${ENVIRONMENT_NAME}.env" ;
PARAM="";
ARTIFACT_STORE="fwd-moments-serverless-api"
ARTIFACT_PATH="${PREFIX}-apis"
SECRET_NAME="fwd-moments-${PREFIX}-api-secret"
AWS_CF_STACK_NAME="${PREFIX}-app-apis"

echo "Prepare releasing ${AWS_CF_STACK_NAME}:";
printSeparator

echo "Get Secret Id from SecretManager, secret name: ${SECRET_NAME}:"
SECRET_VERSION_ID=$(getSecretVersion $SECRET_NAME $AWS_REGION_NAME $AWS_CLI_PROFILE);
if [ "${SECRET_VERSION_ID}" == "" ]; 
then
  printListItem "Invalid SECRET_VERSION_ID or empty...";
  exit 0;
fi
echo "Get Secret Id success..."
printSeparator

echo "Build Parameters:"
printListItem "COUNTRY_CODE = ${COUNTRY_CODE}"
printListItem "ENVIRONMENT_NAME = ${ENVIRONMENT_NAME}"
printListItem "AWS_REGION_NAME = ${AWS_REGION_NAME}"
printListItem "SECRET_NAME = ${SECRET_NAME}"
printListItem "SECRET_VERSION_ID = ${SECRET_VERSION_ID}"
printListItem "PROJECT_PATH = ${PROJECT_PATH}"
printSeparator

echo "SAM Overrided Parameters:"
verifyEnv "$SOURCE_FILE";
getEnv "$SOURCE_FILE";
printSeparator

if [[  ${SLIENT} != "1"  ]]; then
  echo "Are you sure you want to proceed? ([Y]es, [N]o): "
  read CONFIRM_CODE
  if [[ $CONFIRM_CODE == "Y" ]] || [[ $CONFIRM_CODE == "y" ]]; then
    printSeparator
    printListItem "Initiating deployment...";
    printSeparator
  else
    printListItem "Stack release aborted. Press any key to exit";
    read
    clear;
    exit 0;
  fi
fi

cd $PROJECT_PATH;

rm -f apis.cfn.yaml;

echo "Initiating SAM Package:"


sam package --template-file ./serverless.template \
--output-template-file ./apis.cfn.yaml \
--image-repository 607410213812.dkr.ecr.ap-southeast-1.amazonaws.com/test \
--profile moments

sam deploy --template-file ./apis.cfn.yaml \
	--stack-name "test-docker" \
	--capabilities CAPABILITY_IAM \
	--no-fail-on-empty-changeset \
  --image-repository 607410213812.dkr.ecr.ap-southeast-1.amazonaws.com/test \
--profile moments


sam package --template-file ./apis.sam.yaml \
	--s3-bucket "${ARTIFACT_STORE}" \
	--s3-prefix "${ARTIFACT_PATH}" \
	--region "${AWS_REGION_NAME}" \
	--output-template-file ./apis.cfn.yaml \
  $USE_PROFILE
printSeparator

echo "Initiating SAM Deploy:"
sam deploy --template-file ./apis.cfn.yaml \
	--s3-bucket "${ARTIFACT_STORE}" \
	--s3-prefix "${ARTIFACT_PATH}" \
	--region "${AWS_REGION_NAME}" \
	--stack-name "${AWS_CF_STACK_NAME}" \
	--parameter-overrides \
	$PARAM \
	--capabilities CAPABILITY_IAM \
	--no-fail-on-empty-changeset \
  $USE_PROFILE
printSeparator